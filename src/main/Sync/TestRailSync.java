package Sync;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.codepine.api.testrail.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class TestRailSync {

    private static Logger logger = Logger.getLogger(TestRailSync.class);

    @Parameter(names ={"-action"}, description = "Action to execute")
    private String action ="generateMochaTests";

    @Parameter(names={"-projectName"}, description = "TestRail project name")
    private String projectName;
    
    @Parameter(names={"-suiteNames"}, description = "TestRail suite names (comma separated)")
    private String suiteNames;

    @Parameter(names={"-sectionNames"}, description = "TestRail section/subsection names (comma separated), optional, default all sections in suite")
    private String sectionNames;

    @Parameter(names = { "--help", "-h" }, description = "Prints this help", help = true)
    private static boolean help = false;

    public static void main(String[] args) {

        TestRailSync testRailSync = new TestRailSync();

        JCommander jct = JCommander.newBuilder()
                .addObject(testRailSync)
                .build();
        jct.parse(args);

        if (isHelp()) {
            jct.usage();
            return;
        }

        testRailSync.run(new Config());
    }

    public static boolean isHelp() {
        return help;
    }

    private void run(Config config)
    {
        TestRailAPIWrapper wrapper = new TestRailAPIWrapper(config);

        if(projectName==null) {
            logger.error("Project name must be specified, exiting");
            System.exit(1);
        }

        Project proj = wrapper.getProjectByName(projectName);

        if(action.equals("generateMochaTests")){

            if(suiteNames==null) {
                logger.error("Suite name must be specified, exiting");
                System.exit(1);
            }

            MochaGenerator generator = new MochaGenerator(config);
            logger.info(String.format("Generating Mocha spec files from suites %s",suiteNames));

            if(sectionNames==null){
                logger.info("No sections specified, all tests of all sections in suite will be retrieved (recursive)");
            }else{
                logger.info(String.format("Only tests in the following sections/subsections will be retrieved: %s",sectionNames));
            }

            List<Case> allCases = new ArrayList<>();
            String[] splittedSuiteNames = suiteNames.split(",");

            for(String splittedSuiteName : splittedSuiteNames) {
                splittedSuiteName = splittedSuiteName.trim();
                Suite suite = wrapper.getSuiteByName(proj, splittedSuiteName);
                List<Case> tempCases = wrapper.getCasesInSuite(proj, suite);

                if(sectionNames!=null){
                    String[] splittedSectionNames = sectionNames.split(",");
                    for(String splittedSectionName : splittedSectionNames){
                        splittedSectionName = splittedSectionName.trim();
                        Section section = wrapper.getSectionByName(proj, suite, splittedSectionName);
                        if(section!=null){
                            for(Case tempCase : tempCases){
                                if(tempCase.getSectionId()==section.getId()){
                                    allCases.add(tempCase);
                                }
                            }
                        }
                    }
                }else{
                    allCases.addAll(tempCases);
                }

                logger.info(String.format("Specs for %s cases will be generated",allCases.size()));
                Map<Section,List<Case>> casesBySection = wrapper.groupCasesBySection(allCases);
                List<Section> sectionsInSuite = wrapper.getSectionsInSuite(proj, suite);

                generator.generateMochaSpecs(casesBySection,sectionsInSuite);
            }
        }else {
                logger.error("Unrecognized action, exiting");
                System.exit(1);
        }
    }
}
