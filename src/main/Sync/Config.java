package Sync;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Config {

    private static Logger logger = Logger.getLogger(Config.class);
    private static Properties props;

    public Config()
    {
        props = new Properties();
        URL filePath = this.getClass().getClassLoader().getResource("Properties");
        File propertiesDir = null;
        try {
            propertiesDir = new File(filePath.toURI());
        } catch (URISyntaxException e) {
            logger.error(String.format("Couldn't open properties dir, %s, exiting",e.getMessage()));
            System.exit(1);
        }
        List<File> propertiesFiles = Arrays.asList(propertiesDir.listFiles());

        for (File propertiesFile : propertiesFiles) {
            try {
                InputStream inputStream = new FileInputStream(propertiesFile);
                props.load(inputStream);
            } catch (Exception e) {
                logger.error(String.format("Couldn't open properties file %s, %s, exiting",propertiesFile.getName(),e.getMessage()));
                System.exit(1);
            }
        }
    }

    public String getProperty(String propName)
    {
        try {
            return props.getProperty(propName);
        }catch (Exception e){
            logger.error(String.format("Couldn't find property %s, exiting", propName));
            System.exit(1);
        }
        return null;
    }
}
