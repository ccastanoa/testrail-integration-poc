package Sync;

import com.codepine.api.testrail.TestRail;
import com.codepine.api.testrail.model.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import org.apache.log4j.Logger;

public class TestRailAPIWrapper {

    private static Logger logger = Logger.getLogger(TestRailAPIWrapper.class);
    private static TestRail api;
    private static List<CaseField> caseFields;

    private static String testRailURL;
    private static String testRailUsername;
    private static String testRailPassword;

    @SuppressWarnings("rawtypes")
    RetryFunction retryier;

    @SuppressWarnings("rawtypes")
    public TestRailAPIWrapper(Config config)
    {
        testRailURL = config.getProperty("tr.rest.url");
        testRailUsername = config.getProperty("tr.rest.username");
        testRailPassword = config.getProperty("tr.rest.password");
        retryier = new RetryFunction(10,5);
        connect();
    }

    @SuppressWarnings("unchecked")
    void connect()
    {
        try {
            api = (TestRail) retryier.run(()->TestRail.builder(testRailURL, testRailUsername, testRailPassword).build());
        } catch (Exception e) {
            logger.error(String.format("Couldn't login in TestRail instance %s, exiting, please retry", testRailURL));
        }

        caseFields = (List<CaseField>) retryier.run(()->api.caseFields().list().execute());
    }

    @SuppressWarnings("unchecked")
    Project getProjectByName(String projectName)
    {
        logger.info(String.format("Getting project with name %s",projectName));
        List<Project> projects = (List<Project>) retryier.run(()->api.projects().list().execute());

        for(Project project : projects){
            if(project.getName().toLowerCase().equals(projectName.toLowerCase())){
                return project;
            }
        }
        logger.error(String.format("No project with name %s, exiting",projectName));
        System.exit(1);
        return null;
    }

    @SuppressWarnings("unchecked")
    Suite getSuiteByName(Project project, String suiteName)
    {
        logger.info(String.format("Getting suite with name %s",suiteName));
        List<Suite> suites = (List<Suite>) retryier.run(()->api.suites().list(project.getId()).execute());

        for(Suite suite: suites){
            if(suite.getName().toLowerCase().equals(suiteName.toLowerCase())){
                return suite;
            }
        }
        logger.error(String.format("No suite with name %s in project, exiting",suiteName));
        System.exit(1);
        return null;
    }

    @SuppressWarnings("unchecked")
    Section getSectionByName(Project project, Suite suite, String sectionName)
    {
        List<Section> sections = (List<Section>) retryier.run(()->api.sections().list(project.getId(),suite.getId()).execute());

        for(Section section: sections){
            if(section.getName().toLowerCase().equals(sectionName.toLowerCase())){
                return section;
            }
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    List<Case> getCasesInSuite(Project project, Suite suite)
    {
        logger.info(String.format("Getting cases for project %s and suite %s",project.getId(),suite.getId()));
        List<Case> cases = (List<Case>) retryier.run(()->api.cases().list(project.getId(), suite.getId(), caseFields).execute());

        int numberOfCases = cases.size();
        if(numberOfCases==0){
            logger.error(String.format("No cases in suite %s, exiting",suite.getId()));
            System.exit(1);
        }else{
            logger.info(String.format("Retrieved %s cases in suite",numberOfCases));
        }
        return cases;
    }

    @SuppressWarnings("unchecked")
    List<Case> getCasesFromTests(List<Test> tests)
    {
        logger.info("Getting test case information for run");
        List<Case> cases = new ArrayList<>();

        for(Test test : tests){
            logger.info(String.format("\tGetting test case information for test %s",test.getCaseId()));
            Case currentCase = (Case) retryier.run(()->api.cases().get(test.getCaseId(),caseFields).execute());
            cases.add(currentCase);
        }
        return cases;
    }

    @SuppressWarnings("unchecked")
    Map<Section,List<Case>> groupCasesBySection(List<Case> cases)
    {
        Map<Section,List<Case>> sections = new LinkedHashMap<>();

        for(Case tc: cases){
            int sectionId = tc.getSectionId();

            if(sections.keySet().stream().map(Section::getId).noneMatch(id -> (id==sectionId))){
                List<Case> tcs = new ArrayList<>();
                tcs.add(tc);
                Section sec = (Section) retryier.run(()->api.sections().get(sectionId).execute());
                sections.put(sec,tcs);
            }else{
                Section targetSection =sections.keySet().stream().filter(section -> sectionId==section.getId()).findAny().orElse(null);
                sections.get(targetSection).add(tc);
            }
        }
        return sections;
    }

    @SuppressWarnings("unchecked")
    public List<Section> getSectionsInSuite(Project project, Suite suite)
    {    
        return (List<Section>) retryier.run(()->api.sections().list(project.getId(), suite.getId()).execute());
    }

}

class RetryFunction<T> {

    private static Logger logger = Logger.getLogger(RetryFunction.class);
    private int retryCounter;
    private int maxRetries;
    private int sleepMilliseconds;

    public RetryFunction(int maxRetries, int sleepSeconds)
    {
        this.maxRetries = maxRetries;
        this.sleepMilliseconds = sleepSeconds*1000;
    }

    public T run(Supplier<T> function) {
        try {
            return function.get();
        } catch (Exception e) {
            return retry(function);
        }
    }

    public int getRetryCounter() { return retryCounter; }

    private T retry(Supplier<T> function) throws RuntimeException {

        logger.error("FAILED - Command failed, will be retried " + maxRetries + " times.");
        retryCounter = 0;
        while (retryCounter < maxRetries) {
            try {
                return function.get();
            } catch (Exception ex) {
                retryCounter++;
                logger.error("FAILED - Command failed on retry " + retryCounter + " of " + maxRetries + " error: " + ex );
                try {
                    Thread.sleep(sleepMilliseconds);
                } catch (InterruptedException e) {
                    logger.error("FAILED - Catastrophic error while trying to sleep");
                }
                if (retryCounter >= maxRetries) {
                    logger.info("Max retries exceeded.");
                    break;
                }
            }
        }
        throw new RuntimeException("Command failed on all of " + maxRetries + " retries");
    }
}


