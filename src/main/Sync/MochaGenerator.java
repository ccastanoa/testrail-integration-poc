package Sync;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.codepine.api.testrail.model.Case;
import com.codepine.api.testrail.model.Field;
import com.codepine.api.testrail.model.Section;

import org.apache.log4j.Logger;

public class MochaGenerator {

    private static Logger logger = Logger.getLogger(MochaGenerator.class);
    
    private String rootDirPath;
    private String fileExtension;
    private String header;
    private String beforeAllFormat;
    private String beforeEachFormat;
    private String afterAllFormat;
    private String afterEachFormat;
    private String describeFormat;
    private String contextFormat;
    private String itFormat;
    private String tabulator;
    private String closingStatement;

    private String beforeAllKeyword;
    private String beforeEachKeyword;
    private String afterAllKeyword;
    private String afterEachKeyword;

    private String precondition;
    private String action;
    private String expected;
    private String and;

    private String delimiter;

    public MochaGenerator(Config config)
    {
        rootDirPath = config.getProperty("tr.fs.rootFolder");
        fileExtension = config.getProperty("tr.fs.fileExtension");
        header = config.getProperty("tr.spec.header");
        beforeAllFormat = config.getProperty("tr.spec.beforAllFormat");
        beforeEachFormat = config.getProperty("tr.spec.beforeEachFormat");
        afterAllFormat = config.getProperty("tr.spec.afterAllFormat");
        afterEachFormat= config.getProperty("tr.spec.afterEachFormat");
        describeFormat = config.getProperty("tr.spec.describeFormat");
        contextFormat = config.getProperty("tr.spec.contextFormat");
        itFormat = config.getProperty("tr.spec.itFormat");
        tabulator = config.getProperty("tr.spec.tabulator");
        closingStatement = config.getProperty("tr.spec.closingStatement");
        beforeAllKeyword = config.getProperty("tr.spec.beforeAllKeyword");
        beforeEachKeyword = config.getProperty("tr.spec.beforeEachKeyword");
        afterAllKeyword = config.getProperty("tr.spec.afterAllKeyword");
        afterEachKeyword = config.getProperty("tr.spec.afterEachKeyword");
        precondition = config.getProperty("tr.bdd.keyword.precondition");
        action = config.getProperty("tr.bdd.keyword.action");
        expected = config.getProperty("tr.bdd.keyword.expected");
        and = config.getProperty("tr.bdd.keyword.and");
        delimiter = config.getProperty("tr.spec.delimiter");
    }

    public void generateMochaSpecs(Map<Section,List<Case>> sectionsCases, List<Section> sections)
    {
        rootDirPath+="_"+System.currentTimeMillis();
        createDirAt(rootDirPath, "");

        for(Map.Entry<Section, List<Case>> sectionCases : sectionsCases.entrySet()){ 
            List<String> dirStructure = new ArrayList<>();
            getRecursiveDirStructure(sectionCases.getKey(),sections,dirStructure);
            File specFile = generateFileStructureForSection(rootDirPath,dirStructure);
            
            generateSpecSkeleton(sectionCases,dirStructure.get(dirStructure.size()-1),specFile);  
        }
    }

    private File generateFileStructureForSection(String rootPath, List<String> dirStructure)
    {
        File current = createDirAt(rootPath, "");

        if(dirStructure.size()>1){
            for(int i=0; i<dirStructure.size()-2; i++){
                current = createDirAt(current.getAbsolutePath(), dirStructure.get(i));
            }
            return createFileAt(current.getAbsolutePath(), dirStructure.get(dirStructure.size()-2)+fileExtension);
        }else{
            return createFileAt(current.getAbsolutePath(), dirStructure.get(dirStructure.size()-1)+fileExtension);
        }
    }

    private void generateSpecSkeleton(Entry<Section, List<Case>> sectionCases, String parentSectionName, File specFile)
    {
        try {
            FileWriter writer = new FileWriter(specFile,true);
            writer.write(header+System.lineSeparator()+System.lineSeparator());
            writer.write(String.format(describeFormat, sectionCases.getKey().getName())+System.lineSeparator());

            for(int i=0; i<sectionCases.getValue().size(); i++){
                Case current = sectionCases.getValue().get(i);

                String caseTitle = current.getTitle();

                if(caseTitle.contains(beforeAllKeyword)){
                    writeSpecial(current, writer, beforeAllFormat);
                }else if(caseTitle.contains(beforeEachKeyword)){
                    writeSpecial(current, writer, beforeEachFormat);
                }else if(caseTitle.contains(afterAllKeyword)){
                    writeSpecial(current, writer, afterAllFormat);
                }else if(caseTitle.contains(afterEachKeyword)){
                    writeSpecial(current, writer, afterEachFormat);
                }else{
                    writer.write(tabulator+String.format(contextFormat, current.getTitle())+System.lineSeparator());
                    writeTestPreconditions(current,writer);
                    writeStepActionsAndExpects(current,writer);
                    writer.write(tabulator+closingStatement+System.lineSeparator());
                }
            }
            writer.write(closingStatement+System.lineSeparator());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            logger.error(String.format("Couldn't write spec file with error %s",e.getMessage()));
        }
    }

    private List<String> getRecursiveDirStructure(Section section, List<Section> sections, List<String> dirStructure)
    {
        if(section.getParentId()==null){
            dirStructure.add(section.getName());
            return dirStructure;
        }else{
            getRecursiveDirStructure(getParentSection(section, sections), sections, dirStructure);
            dirStructure.add(section.getName());
            return dirStructure;
        }
    }

    private Section getParentSection(Section section, List<Section> sections)
    {
        return sections.stream().filter(p-> p.getId() == section.getParentId()).findFirst().orElse(null);
    }

    private void writeTestPreconditions(Case tc, FileWriter writer) throws IOException
    {
        if(tc.getCustomFields().get("preconds")!=null && tc.getCustomFields().get("preconds")!=""){
            String rawPreconditions = ((String) tc.getCustomFields().get("preconds")).trim();
            String [] parsedPreconds = rawPreconditions.split("\n");

            for(int i=0; i< parsedPreconds.length; i++){
                if(i==0){
                    writer.write(tabulator+tabulator+String.format(itFormat,
                        String.format(precondition,escapeDelimiters(parsedPreconds[i].trim())))+closingStatement+System.lineSeparator());
                }else{
                    writer.write(tabulator+tabulator+String.format(itFormat,
                        String.format(and,escapeDelimiters(parsedPreconds[i].trim())))+closingStatement+System.lineSeparator());
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void writeStepActionsAndExpects(Case tc, FileWriter writer) throws IOException
    {
        if(tc.getCustomFields().get("steps_separated")!=null && tc.getCustomFields().get("steps_separated")!=""){

            List<Field.Step> separatedSteps = (List<Field.Step>) tc.getCustomFields().get("steps_separated");
            
            for(int i=0; i< separatedSteps.size(); i++){
                Field.Step step = separatedSteps.get(i);

                String rawAction = step.getContent();
                String[] parsedAction = rawAction.split("\n");

                for (int ii = 0; ii < parsedAction.length; ii++) {
                    if (ii == 0) {
                        writer.write(tabulator+tabulator+String.format(itFormat,
                            String.format(action, escapeDelimiters(parsedAction[ii].trim())))+closingStatement+System.lineSeparator());
                    } else {
                        writer.write(tabulator+tabulator+String.format(itFormat,
                            String.format(and, escapeDelimiters(parsedAction[ii].trim())))+closingStatement+System.lineSeparator());
                    }
                }

                if (step.getExpected() != null && !step.getExpected().equals("")) {
                    String rawExpected = step.getExpected();
                    String[] parsedExpected = rawExpected.split("\n");

                    for (int ii = 0; ii < parsedExpected.length; ii++) {
                        if (ii == 0) {
                            writer.write(tabulator+tabulator+String.format(itFormat,
                                String.format(expected, escapeDelimiters(parsedExpected[ii].trim())))+closingStatement+System.lineSeparator());
                        } else {
                            writer.write(tabulator+tabulator+String.format(itFormat,
                                String.format(and, escapeDelimiters(parsedExpected[ii].trim())))+closingStatement+System.lineSeparator());
                        }
                    }
                }
            }
        }
    }

    private void writeSpecial(Case tc, FileWriter writer, String descriptor) throws IOException
    {
        if(tc.getCustomFields().get("preconds")!=null && tc.getCustomFields().get("preconds")!=""){
            String rawPreconditions = ((String) tc.getCustomFields().get("preconds")).trim();
            String [] parsedPreconds = rawPreconditions.split("\n");

            for(int i=0; i< parsedPreconds.length; i++){
                if(i==0){
                    writer.write(tabulator+String.format(descriptor, escapeDelimiters(parsedPreconds[i].trim()))+closingStatement+System.lineSeparator());
                }
            }
        }
    }

    private File createDirAt(String path, String targetDir)
    {
        File dir = new File(Paths.get(path,targetDir).toString());

        if (! dir.exists()){
            dir.mkdir();
        }
        return dir;
    }

    private File createFileAt(String path, String targetFile)
    {
        File file = new File(Paths.get(path,targetFile).toString());

        if (! file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                logger.error(String.format("Couldn't create spec file with error %s",e.getMessage()));
            }
        }
        return file;
    }

    private String escapeDelimiters(String input)
    {
        String escaped = input;
        if(delimiter.equals("SINGLE")){
            return input.replace("'", "\\'");
        }else if(delimiter.equals("DOUBLE")){
            return input.replace("\"","\\\"");
        }else{
            return escaped;
        }
    }
}