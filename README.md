# TestRail Integration POC #

This software provides the functionality of generating valid Mocha-Cypress tests structures from TestRail test suites reflecting the section structure. This software is provided as an executable JAR file.

For more information on the entity mapping between TestRail and Cypress-Mocha visit [this document](https://bigfinite.atlassian.net/wiki/spaces/EQC/pages/3012001834/TestRail+integration+PoC#Mapping-definition)

This repository contains a compiled ready-to-use version of the software at [testrail-sync-package](target/testrail-sync-package) folder

## Configuration

In order to configure the tool file [testRail.properties](target/testrail-sync-package/Properties/testRail.properties) needs to be modified. The relevant parameters are

````
tr.rest.username={TestRail username}
tr.rest.password={TestRailPassword}
tr.fs.rootFolder={Absolute path of the directory where the spec files are going to be generated}
````

## Execution

Executable JAR is provided togheter with a shell script that will set the classpath and execute the tool. File is TestRailSync.sh and can be found in the tool directory. The tool takes the following parameters:

* -projectName: Name of the TestRail project (as it appears in the interface)
* -suiteNames: Comma separated list of the project suite names containing the tests to be downloaded (as they appear in the interface)
* -sectionNames: Comma separated list of section names containing the tests to be downloaded (as they appear in the interface), optional, if not specified all tests of all suites will be downloaded
* -help: Prints help

### Examples

`````
./TestRailSync.sh -projectName "Sandbox" -suiteNames "Sandbox"
`````
`````
./TestRailSync.sh -projectName "Sandbox" -suiteNames "Sandbox" -sectionNames "Successfull authentication with username and password for admins, userPassErrorLogin"
`````

## Tips

* There is no need to include BDD keywords (as Gherkin GIVEN, WHEN, THEN or Mocha "should") as the tool includes them automatically in the generated tests structure. Keywords can be modified or deleted in config file [testRail.properties](target/testrail-sync-package/Properties/testRail.properties).They can also be supressed, in order to do so remove the keyword from the config (leave %s only). Keys are:
    * tr.bdd.keyword.precondition: TestRail test preconditions: 
    * tr.bdd.keyword.action: TestRail test step action: 
    * tr.bdd.keyword.expected: TestRail test step expected result: 
* To create before and after conditions in Mocha structure the tool searches for special keywords in test case title. Keywords can be configured and mapped to Mocha's before(), after(), beforeEach() and afterEach(). Keywords can be modified in the config file [testRail.properties](target/testrail-sync-package/Properties/testRail.properties). Keys are:
    * tr.spec.beforeAllKeyword: Keyword to map to Mocha before()
    * tr.spec.beforeEachKeyword: : Keyword to map to Mocha beforeEach()
    * tr.spec.afterAllKeyword: Keyword to map to Mocha after()
    * tr.spec.afterEachKeyword: Keyword to map to Mocha afterEach()
